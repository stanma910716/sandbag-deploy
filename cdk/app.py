#!/usr/bin/env python3

import os.path
import base64

from aws_cdk.aws_s3_assets import Asset
from aws_cdk import (
    aws_ec2 as ec2,
    aws_iam as iam,
<<<<<<< HEAD
    aws_ecs as ecs,
    aws_elasticloadbalancingv2 as elb,
=======
    aws_elasticache as elasticache,
    aws_autoscaling as asg,
>>>>>>> master
    core
)

dirname = os.path.dirname(__file__)
request_server_num = 1

class EC2Stack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        amzn_linux = ec2.MachineImage.latest_amazon_linux(
            generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX,
            edition=ec2.AmazonLinuxEdition.STANDARD,
            virtualization=ec2.AmazonLinuxVirt.HVM,
            storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
        )

        vpc = ec2.Vpc(
            self,
            "vpc-sandbag",
            subnet_configuration = [ec2.SubnetConfiguration(name="",subnet_type=ec2.SubnetType.PUBLIC)]
        )

        vpc_subnets = vpc.select_subnets(
            subnet_type=ec2.SubnetType.PUBLIC
        ).subnet_ids

        # vpc_subnets = vpc.select_subnets(
        #     subnet_type=ec2.SubnetType.PUBLIC
        # ).subnets

        web_sg = ec2.SecurityGroup(
            self,
            "SG-SandBag",
            vpc=vpc,
            security_group_name="SandBag_SG"
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(80)
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(22)
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(6379)
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp_range(
                start_port=1024,
                end_port=65535
            )
        )
        role = iam.Role(
            self,
            "Role-SandBag",
            assumed_by=iam.ServicePrincipal("ecs.amazonaws.com")
        )
        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonDynamoDBFullAccess"))
        
        redis_task_definition = ecs.FargateTaskDefinition(self, "SandBagRedisTask",
            memory_limit_mib=1024,
            cpu=512
        )
        dashboard_task_definition = ecs.FargateTaskDefinition(self,"SandBag-DashBoardTask",
            memory_limit_mib=512,
            cpu=256
        )
        user_dashboard_task_definition = ecs.FargateTaskDefinition(self,"SandBag-UserDashBoardTask",
            memory_limit_mib=512,
            cpu=256
        )
        request_task_definition = ecs.FargateTaskDefinition(self,"SandBag-RequestServerTask",
            memory_limit_mib=512,
            cpu=256
        )
        redis_container = redis_task_definition.add_container(
            "sandbagRedisContainer",
            image=ecs.ContainerImage.from_asset('./docker/redis')
        )
        dashboard_container = dashboard_task_definition.add_container(
            "sandbagDashBoardContainer",
            image=ecs.ContainerImage.from_asset('./docker/dashboard')
        )
        userdashboard_container = user_dashboard_task_definition.add_container(
            "sandbagDashBoardContainer",
            image=ecs.ContainerImage.from_asset('./docker/user_dashboard')
        )
        request_server_container = request_task_definition.add_container(
            "sandbagRequestServer",
            image=ecs.ContainerImage.from_asset('./docker/request_server')
        )
        redis_container.add_port_mappings(
            ecs.PortMapping(
                container_port=6379,
                host_port=6379
            )
        )
        dashboard_container.add_port_mappings(
            ecs.PortMapping(
                container_port=80,
                host_port=80
            )
        )
<<<<<<< HEAD
        userdashboard_container.add_port_mappings(
            ecs.PortMapping(
                container_port=80,
                host_port=80
            )
=======

        ec_security.node.add_dependency(ec_sg)

        ec = elasticache.CfnReplicationGroup(
            self,
            "SandBag - Redis",
            cache_node_type="cache.t3.micro",
            engine="redis",
            num_node_groups=1,
            replicas_per_node_group=2,
            replication_group_description="Read Replica",
            security_group_ids=[web_sg.security_group_id],
            cache_subnet_group_name=ec_sg.ref
        )
        ec.node.add_dependency(ec_security)

        rs_ag = asg.AutoScalingGroup(
            self,
            "Request Server",
            instance_type=ec2.InstanceType("t2.micro"),
            machine_image=amzn_linux,
            vpc=vpc,
            role=role,
            security_group=web_sg,
            min_capacity=request_server_num
>>>>>>> master
        )
        request_server_container.add_port_mappings(
            ecs.PortMapping(
                container_port=80,
                host_port=80
            )
        )

        cluster = ecs.Cluster(self, "Cluster",
            vpc=vpc
        )

        redis_service = ecs.FargateService(
            self,"RedisServer",
            cluster=cluster,
            task_definition=redis_task_definition,
            assign_public_ip=True,
            security_group=web_sg
        )

        dashboard_service = ecs.FargateService(
            self,"DashBoardServer",
            cluster=cluster,
            task_definition=dashboard_task_definition,
            assign_public_ip=True,
            security_group=web_sg
        )
        user_dashboard_service = ecs.FargateService(
            self,"UserDashBoardServer",
            cluster=cluster,
            task_definition=user_dashboard_task_definition,
            assign_public_ip=True,
            security_group=web_sg
        )
        request_server_service = ecs.FargateService(
            self,"RequestServer",
            cluster=cluster,
            task_definition=request_task_definition,
            assign_public_ip=True,
            security_group=web_sg,
            desired_count=request_server_num
        )

        
        request_server_service.task_definition.add_to_task_role_policy(
            iam.PolicyStatement(
                effect=iam.Effect.ALLOW,
                actions=["dynamodb:*"],
                resources=["*"]
            )
        )
        request_server_service.node.add_dependency(redis_service)


app = core.App()

EC2Stack(app, "SandBag")

app.synth()