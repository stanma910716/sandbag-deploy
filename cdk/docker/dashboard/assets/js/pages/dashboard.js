$(document).ready(function(){
    Codebase.helpers([
        "rangeslider"
    ])
    Codebase.blocks("#request_enable_block","state_loading")
    Codebase.blocks("#request_interval_block","state_loading")
    Codebase.blocks("#waf_enable_block","state_loading")
    Codebase.blocks("#request_version_block","state_loading")
    // Codebase.blocks("#request_enable_block","state_loading")
})
$("#update_endpoint_btn").click(function (){
    let post_data = {
        "action":"update_endpoint",
        "token":sessionToken,
        "endpoint":$("#endpoint_field").val()
    }
    ws.send(JSON.stringify(post_data));
})
$("#event-edit-btn").click(function(){
    let post_data = {
        "action" : "get_event",
        "token" : sessionToken
    }
    post_data = JSON.stringify(post_data)
    ws.send(post_data);
})
$("#event-update-btn").click(function(){
   let post_data = {
       "action": "update_event",
       "token": sessionToken,
       "eid": $("#event_id").val(),
       "ename": $("#event_name").val()
   }
   post_data = JSON.stringify(post_data);
   ws.send(post_data)
});
$("#view-users-btn").click(function (){
    users_popup()
});
function users_popup(){
    let post_data = {
        "action":"get_users",
        "token":sessionToken
    }
    post_data = JSON.stringify(post_data);
    ws.send(post_data)
    $("#modal-users-list").modal("show");
}
$("#add_user_popup").click(function(){
    $("#modal-users-list").modal("hide");
    $("#add_user_num").val("1");
    $("#modal-user-add").modal("show");
})
function del_user(element){
    let post_data = {
        "action":"delete_user",
        "token":sessionToken,
        "del_token":$(element).attr("data-token")
    }
    post_data = JSON.stringify(post_data)
    ws.send(post_data)
};
function add_user(add){
    if(add == true){
        let add_user_num = $("#add_user_num").val();
        $("#add_user_num").val("1")
        $("#modal-user-add").modal("hide");
        let post_data = {
            "action":"new_user",
            "token":sessionToken,
            "user_num":add_user_num
        }
        post_data = JSON.stringify(post_data)
        ws.send(post_data);
    }else{
        $("#modal-user-add").modal("hide");
        users_popup()
    }
}
$("input[type=radio]").change(function(){
    let post_data = {
        "action":"update_admin",
        "token":sessionToken,
        "key":$(this).attr("data-key"),
        "value":$(this).attr("data-value")
    }
    post_data = JSON.stringify(post_data)
    ws.send(post_data)
})
$("#request-interval").change(function(){
    let post_data = {
        "action":"update_admin",
        "token":sessionToken,
        "key":"request_interval",
        "value":$(this).val()
    }
    post_data = JSON.stringify(post_data)
    ws.send(post_data)
})
$("#request_version").change(function(){
    let post_data = {
        "action":"update_admin",
        "token":sessionToken,
        "key":"request_version",
        "value":$(this).val()
    }
    post_data = JSON.stringify(post_data)
    ws.send(post_data)
})