var data = [
    {
        token: "2f876dfd-7a4c-40d5-a314-8b364197445e",
        team_name: "Unknown Team",
        score: 0
    },
    {
        token: "2f876dfd-7a4c-40d5-a314-8b364197445a",
        team_name: "Unknown Team",
        score: 20
    },
    {
        token: "2f876dfd-7a4c-40d5-a314-8b364197445b",
        team_name: "Unknown Team",
        score: -20
    },
];
data.sort(function(a, b) {
    return  b["score"] - a["score"];
});