const sessionToken = window.localStorage.getItem("SessionToken");
var ws_open = 0;

if(sessionToken === undefined || sessionToken == null){
    window.location.href = '/';
}

function logout(){
    window.localStorage.removeItem("SessionToken")
    window.location.href = '/';
}

const ws = new WebSocket(
    websocket_url
);
ws.onopen = function(){
    let post_data = JSON.stringify({"action":"login","token":sessionToken});
    ws.send(post_data);
    ws_open = 1;
}
ws.onmessage = function (evt) {
    let received_msg = JSON.parse(evt.data);
    switch (received_msg.action){
        case "login":
            if(received_msg.success === true && received_msg.role === "admin"){
                // logged in
                $("#teamId").html(sessionToken)
                $("#team_name").html("admin")
                $("#set_team_name").remove();
                $("#set-team-name-dialog").remove();
                page_ws();
            }else{
                // session error
                logout()
            }
            break;
        case "get_event":
            $("#event_name").val(received_msg.event_name);
            $("#event_id").val(received_msg.event_id);
            $("#modal-event-edit").modal("show");
            break;
        case "update_endpoint":
            if(received_msg.success === true){
                Swal.fire(
                    "Success",
                    "Everything updated perfectly!",
                    "success"
                )
                $("#endpoint_field").val(received_msg.endpoint);
            }else{
                Swal.fire(
                    "Error",
                    "Nothing update!",
                    "danger"
                )
                $("#endpoint_field").val("");
            }
        case "get_admin":
            $("#request_server[value="+received_msg.request_power+"]").prop("checked",true);
            $('#request-interval').data("ionRangeSlider").update({
                from:received_msg.request_interval
            });
            $("#waf_request[value="+received_msg.waf_power+"]").prop("checked",true);
            $("#request_version").val(received_msg.request_version)
            Codebase.blocks("#request_enable_block","state_normal")
            Codebase.blocks("#request_version_block","state_normal")
            Codebase.blocks("#request_interval_block","state_normal")
            Codebase.blocks("#waf_enable_block","state_normal")
            break;
        case "update_event":
            if(received_msg.success == true){
                Swal.fire(
                    "Success",
                    "Everything updated perfectly!",
                    "success"
                )
                $("#modal-event-edit").modal("hide");
            }else{
                Swal.fire(
                    "Error",
                    received_msg.Msg,
                    "danger"
                )
                $("#modal-event-edit").modal("hide");
            }
            break;
        case "get_users":
            var users_list = received_msg.users;
            let user_num = 1;
            $("#users-list-detail > tbody").html("")
            $.each(received_msg.users , function(user){
                // console.log(user)
                let available = "True";
                if(users_list[user]["available"] == true){
                    available = "True";
                }else{
                    available = "Used";
                }
                $("#users-list-detail > tbody").append(
                    "<tr id='user-data' data-token='"+user+"'>" +
                    "<td>"+users_list[user]["team_name"]+"</td>" +
                    "<td>" + user + "</td>" +
                    "<td>" + available + "</td>" +
                    "<td class='text-center'>" +
                    "<div class='btn-group'>" +
                    "<button type='button' class='btn btn-sm btn-secondary js-tooltip-enabled' data-toggle='tooltip' title='' data-origin-title='Edit'>" +
                    '<i class="fa fa-pencil"></i>'+
                    "</button>"+
                    "<button type='button' class='btn btn-sm btn-secondary js-tooltip-enabled' onclick='del_user(this)' data-token='"+user+"' data-toggle='tooltip' title='' data-origin-title='Delete'>" +
                    '<i class="fa fa-times"></i>'+
                    "</button>"+
                    "</div>"+
                    "</td>" +
                    "</tr>"
                )
                user_num += 1
                console.log(user+" : "+users_list[user]["team_name"])
            })
            break;
        case "delete_user":
            if(received_msg.success == true){
                Swal.fire(
                    "Success",
                    "User remove perfectly!",
                    "success"
                )
                $("tr[data-token="+received_msg.del_token+"]").remove();
            }else{
                Swal.fire(
                    "Error",
                    "Nothing remove!",
                    "danger"
                )
            }
            break
        case "new_user":
            if(received_msg.success == true){
                Swal.fire(
                    "Success",
                    "Add user perfectly!",
                    "success"
                )
                users_popup()
            }else{
                Swal.fire(
                    "Error",
                    received_msg.Msg,
                    "danger"
                )
            }
        case "scoreboard":
            let data = received_msg.data;
            data.sort(function(a, b) {
                return  b["score"] - a["score"];
            });
            $("#scoreboard-data").html("")
            let rank = 1;
            $.each(data,function(index){
                score_class = "danger";
                if(data[index]["score"] == 0){
                    score_class = "primary";
                }else if(data[index]["score"] > 0){
                    score_class = "success";
                }else{
                    score_class = "danger";
                }
                $("#scoreboard-data").append(
                    "<tr>" +
                    "<td class='text-center' scope='row'>"+rank+"</td>" +
                    "<td>"+data[index]["team_name"]+"</td>" +
                    "<td class='d-none d-sm-table-cell'>" +
                    "<span class='badge badge-"+score_class+"'>"+data[index]["score"]+"</span>" +
                    "</td>"
                )
                rank += 1;
            })
            break;
        default:
            console.log(received_msg);
            break;
    }
};
ws.onclose = function() {
    // websocket is closed.
    window.location.reload()
};
function page_ws(){
    switch (pagename){
        case "DashBoard":
            let post_data = JSON.stringify({"action":"get_admin","token":sessionToken});
            ws.send(post_data);
            break;
        case "ScoreBoard":
            setInterval(function(){
                let post_data = {
                    "action":"scoreboard",
                    "token":sessionToken
                }
                post_data = JSON.stringify(post_data)
                ws.send(post_data);
            },3000)
            break;
    }
}
function go_scoreboard(){
    window.open("/scoreboard/","_blank");
    // window.location.href = "/scoreboard/";
}
function go_history(){
    window.open("/history/","_blank");
}