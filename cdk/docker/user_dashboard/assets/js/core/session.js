const sessionToken = window.localStorage.getItem("SessionToken");
var ws_open = 0;

if(sessionToken === undefined || sessionToken == null){
    window.location.href = '/';
}

function logout(){
    window.localStorage.removeItem("SessionToken")
    window.location.href = '/';
}

const ws = new WebSocket(
    websocket_url
);
ws.onopen = function(){
    let post_data = JSON.stringify({"action":"login","token":sessionToken});
    ws.send(post_data);
    ws_open = 1;
}
ws.onmessage = function (evt) { 
    let received_msg = JSON.parse(evt.data);
    switch (received_msg.action){
        case "login":
            if(received_msg.success === true && received_msg.role != "admin"){
                // logged in
                $("#teamId").html(sessionToken)
                if("team_name" in received_msg && received_msg["team_name"] != null && received_msg["team_name"] != ""){
                    $("#team_name").html(received_msg.team_name)
                    $("#set_team_name").remove();
                    $("#set-team-name-dialog").remove();
                }else{
                    $("#team_name").html("Unknown Team")
                }
                page_ws();
            }else{
                // session error
                window.localStorage.removeItem("SessionToken")
                window.location.href = '/';
            }
            break;
        case "set_team_name":
            if(received_msg.success === true){
                $("#team_name").html(received_msg.team_name)
                $("#set_team_name").remove();
                $("#set-team-name-dialog").modal("hide");
            }else{
                alert("Error Setup!")
            }
            break;
        case "get_event":
            $("#ename").html(received_msg.event_name);
            $("#eid").html(received_msg.event_id);
            $("#module_name").html(received_msg.event_name);
            if("endpoint" in received_msg){
                $("#endpoint_field").val(received_msg.endpoint);
            }
            Codebase.blocks("#event_block","state_normal")
            Codebase.blocks("#module_block","state_normal")
            Codebase.blocks("#scoreboard-block","state_normal")
            Codebase.blocks("#event-history-block","state_normal")
            break;
        case "update_endpoint":
            if(received_msg.success === true){
                Swal.fire(
                    "Success",
                    "Everything updated perfectly!",
                    "success"
                )
                $("#endpoint_field").val(received_msg.endpoint);
            }else{
                Swal.fire(
                    "Error",
                    "Nothing update!",
                    "danger"
                )
                $("#endpoint_field").val("");
            }
            break;
        case "scoreboard":
            let data = received_msg.data;
            data.sort(function(a, b) {
                return  b["score"] - a["score"];
            });
            $("#scoreboard-data").html("")
            let rank = 1;
            $.each(data,function(index){
                score_class = "danger";
                if(data[index]["score"] == 0){
                    score_class = "primary";
                }else if(data[index]["score"] > 0){
                    score_class = "success";
                }else{
                    score_class = "danger";
                }
                $("#scoreboard-data").append(
                    "<tr>" +
                    "<td class='text-center' scope='row'>"+rank+"</td>" +
                    "<td>"+data[index]["team_name"]+"</td>" +
                    "<td class='d-none d-sm-table-cell'>" +
                    "<span class='badge badge-"+score_class+"'>"+data[index]["score"]+"</span>" +
                    "</td>"
                )
                rank += 1;
            })
            break;
        case "now_score":
            Codebase.blocks("#now-score-block","state_normal")
            $("#now_score").html(received_msg.score)
            $("#ename").html(received_msg.event_name);
            $("#eid").html(received_msg.event_id);
            $("#module_name").html(received_msg.event_name);
            break
        case "event_history":
            if(pagename == "History"){
                let innerHTML = "<tr><td style='text-align: center'>"+received_msg.score_change+"</td><td>"+received_msg.Msg+"</td></tr>";
                $("#event-history-data").html(
                    innerHTML+$("#event-history-data").html()
                )
            }
            break;
        default:
            break;
    }
};
ws.onclose = function() {
    // websocket is closed.
};
function page_ws(){
    switch (pagename){
        case "DashBoard":
            let post_data = JSON.stringify({"action":"get_event","token":sessionToken});
            ws.send(post_data);
            setInterval(function(){
                let post_data = {
                    "action":"now_score",
                    "token":sessionToken
                }
                post_data = JSON.stringify(post_data)
                ws.send(post_data);
            },3000);
            break;
        case "ScoreBoard":
            setInterval(function(){
                let post_data = {
                    "action":"scoreboard",
                    "token":sessionToken
                }
                post_data = JSON.stringify(post_data)
                ws.send(post_data);
            },3000)
            break;
    }
}
function go_scoreboard(){
    window.open("/scoreboard/","_blank")
}
function go_history(){
    window.open("/history/","_blank");
}