<?php
// EC2 Meta Data URI : 169.254.169.254
require __DIR__."/vendor/autoload.php";

// env setup
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// twig setup
use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;
$loader = new FilesystemLoader(__DIR__."/templates");
$twig = new Environment($loader,[
//     "cache" => __DIR__."/cache"
]);

$GLOBALS["context"] = [
    "env" => $_ENV
];

// router init
$router = new AltoRouter();

// router setup
$router->map("GET","/",function(){
    global $twig;
    $template = $twig->load("login.twig");
    echo $template->render($GLOBALS["context"]);
});
$router->map("GET","/dashboard/",function(){
    $page_data = [
        "name" => "DashBoard"
    ];
    $GLOBALS["context"]["page"] = $page_data;
    global $twig;
    $template = $twig->load("index.twig");
    echo $template->render($GLOBALS["context"]);
});
$router->map("GET","/scoreboard/",function(){
    $page_data = [
        "name" => "ScoreBoard"
    ];
    $GLOBALS["context"]["page"] = $page_data;
    global $twig;
    $template = $twig->load("scoreboard.twig");
    echo $template->render($GLOBALS["context"]);
});
$router->map("GET","/history/",function(){
    $page_data = [
        "name" => "History"
    ];
    $GLOBALS["context"]["page"] = $page_data;
    global $twig;
    $template = $twig->load("history.twig");
    echo $template->render($GLOBALS["context"]);
});

// match current request url
$match = $router->match();

// call closure or throw 404 status
if( is_array($match) && is_callable( $match['target'] ) ) {
	$GLOBALS["title"] = $match["name"];
	call_user_func_array( $match['target'], $match['params'] ); 
} else {
	// no route was matched
	header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}