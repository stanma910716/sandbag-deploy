import time
import requests
import hashlib
import datetime


"""Check Header score and condition
Attribute :
    judge_score : token -> 100, version -> 50, from_type -> 50
    judge : 0 -> token, 1 -> version, 2 -> from_type 
"""
judge_score = [100,20,20]

judge = {
    0:{
        "correct" : 50, # 100
        "wrong" : -50 # -100
    },
    1:{
        "correct" : 20, # 100
        "wrong" : -20 # -100
    }
}


def transfer(t):
    # Type : str -> int 
    """Transfer time type
    Let the string time which like "2019-8-8 6:30" transfer to integer time which is 
    second based. 
    
    Args :
        t : The time which want to transfer to integer time.  
    """
    transfer_time = time.strptime(t, "%Y-%m-%d %H:%M")
    return int(time.mktime(transfer_time))
        
def transfer_endpoint(endpoint):
    # Type : str -> str 
    """Transfer endpoint 
    It will check the endpoint start with "http", if it doesn't start with http, wll add
    the "http://" in front  the endpoint. 
    
    Args : 
        endpoint : which want to transfer to endpoint with the "http://" .
    
    Return :
        endpoiont : Already transfered endpoint. 
    """
    if(endpoint.startswith("http")):
        return endpoint
    else:
        endpoint = "http://"+endpoint
        return endpoint

def get_header(user_response,endpoint,token,version,request_type):
    # Type : list, str, str -> list 
    now_time = datetime.datetime.now()
    if("http://"  not in endpoint):
        endpoint = "http://"+endpoint
    if(request_type >= 1 and request_type <= 5):
        version = "Refund"
        new_refund_data = "{'refund_token' : "+str(token)+"}"
        refund = requests.post(
            url = "https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/refund",
            data = new_refund_data
        )
        print(refund.text)
        r_header = {'Token':token,'Version':version,"Rtime":str(now_time)}
    elif(request_type >= 81 and request_type <= 100):
        version = "WAF"
        r_header = {'Token':token,'Version':version,'runcmd':'python3 /etc/build.py'}
    else:
        r_header = {'Token':token,'Version':version,"Rtime":str(now_time)}
    """Get header 


    """
    response=requests.get(endpoint,headers=r_header) # sending requests
    if(version == "WAF"):
        send_request = {
            "token" : token,
            "version" : version,
            "send_time" : now_time
        }
        user_response = {
            "status_code" : response.status_code
        }
        return_data = {
            "user":user_response,
            "send":send_request
        }
        return return_data
    user_response=response.headers
    if("restime" in response.headers):
        user_response["response_time"]=datetime.datetime.strptime(response.headers['restime'],'%Y-%m-%d %H:%M:%S.%f')
    user_response["status_code"] = response.status_code
    send_request = {
        "token" : token,
        "version" : version,
        "send_time" : now_time
    }

    return_data = {
        "user":user_response,
        "send":send_request
    }
    return return_data

def get_user(r1):
    # Type : redis -> list 
    """Transfer time type

    """
    user=[]
    for key in r1.scan_iter():
        if( "user" in key):
            username = key.replace("_user", "")
            user.append(username)
    return user

def check_header(id,ans,response,setting_time):
    # Type : int, str, str, int -> int
    """Check the request's header 
    Check the request header is correct or not, using 
    Args :
        id : 
        ans : 
        response :
        setting_time :
    """
    if(id == 0):
        if(ans == response):
            response = "correct"
            return judge[id][response]
        else:
            response = "wrong"
            return judge[id][response]
    elif(id == 1):
        if(ans == response):
            response = "correct"
            return judge[id][response]
        else:
            response = "wrong"
            return judge[id][response]
    elif(id == 2):
        if(response["status_code"] == 403):
            return 400
        else:
            return -400
    elif(id == 3):
        timed = (response-ans)
        second = timed.total_seconds()
        if(second <= 10):
            return 15
        elif(second > 10 and second <= 15):
            return 10
        elif(second > 15 and second <= 20):
            return 5
        else:
            return -10

class User:
    def __init__(self, username, endpoint):
        # Type : str, str -> None
        """
        """
        self.username = username
        self.endpoint = endpoint
        self.score = 0
        self.error_ms = ""
    
    def setUsername(self,user,index):
        # Type : list, int -> None
        """
        """
        username = user[index]
        self.username = username
            
    def setEndpoint(self,r1):
        # Type : redis -> None
        try:
            endpoint = r1.lindex(self.username + "_user",0) 
            endpoint = transfer_endpoint(endpoint)
            self.endpoint = endpoint
        except:
            print("redis error endpoint ")
    def setError(self,user_response,r,give_data):
        # Type : list, redis -> None
        error_ms = (
            "receive token : " + user_response["token"][:6] + "\n" + 
            "version : " + user_response["version"] + "\n" + 
            "type : " + user_response["from_type"]+ "<br>\n" +
            "given&nbsp;&nbsp;&nbsp;token : " + give_data["token"][:6] + "\n" + 
            "version : " + give_data["version"] + "\n" + 
            "type : " + give_data["from_type"]
        )
        # error_ms = ("token : "+ user_response["token"]+"\n"
        #         + "version : "+user_response["version"]+"\n"+ 
        #         "Type : " +user_response["from_type"])
        self.error_ms = error_ms
        try:
            r.lset(self.username + '_user', 1, error_ms) 
        except:
            print("redis error errormessage ")

    def setScore(self,r):
        # redis -> None 
        """
        save the score to the Class and 
        """
        try:
            r.zincrby(name="score", value=self.username, amount=self.score)
        except:
            print("redis error score")