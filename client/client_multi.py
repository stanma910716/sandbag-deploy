import threading 
import json
import redis
import uuid
import requests
from random import randint
from package.module import *

# connect redis

with open('/home/ec2-user/sandbag-deploy/redis.endpoint', 'r') as files:
    redis_endpoint = str(files.read()).replace('[', '').replace(']', '').replace(' ', '').split(',')
r = redis.Redis(host=redis_endpoint[0], port=6379, charset="utf-8",decode_responses=True)
r1 = redis.Redis(host=redis_endpoint[2] , port=6379, charset="utf-8",decode_responses=True) 

# initial variable -- setting time for version and from_type
version_time = transfer("2019-8-8 6:30")
from_type_time = transfer("2019-8-8 6:30")

DEBUG_MODE = False

def stresser(num):
    global all_user 
    index = 0 # 
    last_time = 0 
    delay = 0
    while True:
        while True :
            if( r1.exists("request_delay") ):
                if( r1.get("power") == "off" ):
                    break
            if( index == 0):
                if(r1.exists("request_delay")):
                    if (r1.get("request_delay") != 0):
                        delay = r.get("request_delay")
                        time.sleep(float(r.get("request_delay")))
                else:
                    r.set("request_delay",1.0)
                    time.sleep(1.0)
                print(str(num)+" => Delay Time : "+str(delay))
            # create token
            TOKEN = str(uuid.uuid4())
            Version = r1.get("request_version")
            SCORE = 0

            user_response = { "token" : "","version" : "","from_type" : "" }

            if (time.time() - last_time) > 5:
                last_time = time.time()
                all_user = get_user(r1)

            # create User
            if(r1.get("waf") == "on"):
                request_type = randint(1,80)
            else:
                request_type = randint(1,100)
            if(len(all_user)>0):
                user = User(username="", endpoint="")
                user.setUsername(all_user, index)
                user.setEndpoint(r1)
                
                # get header
                if(user.endpoint == "http://" or user.endpoint == None):
                    break
                data = get_header(user_response,user.endpoint,TOKEN,Version,request_type)
                send_request = data["send"]
                user_response = data["user"]

                Version = send_request["version"]
                
                if(Version == "v1"):
                    TOKEN = TOKEN
                elif(Version == "v2"):
                    encrypt = hashlib.md5()
                    encrypt.update(TOKEN.encode("utf-8"))
                    TOKEN = encrypt.hexdigest()
                elif(Version == "v3"):
                    encrypt = hashlib.sha1()
                    encrypt.update(TOKEN.encode("utf-8"))
                    TOKEN = encrypt.hexdigest()
                elif(Version == "Refund" and Version != "WAF"):
                    TOKEN = TOKEN
                if(Version != "Refund" and Version != "WAF"):
                    given_data = { "token" : TOKEN,"version" : Version,"from_type" : "ECS" }
                    # check header
                    SCORE += check_header(0,TOKEN,user_response["token"],0)
                    SCORE += check_header(1,Version,user_response["version"],version_time)
                    SCORE += check_header(3,send_request["send_time"],user_response["response_time"],0)
                    # SCORE += check_header(2,None,user_response["from_type"],from_type_time)
                    user.score = SCORE
                    if(user_response['token'] == ""):
                        user.setError({'token' : 'Timeout', 'version': '', 'from_type': ''}, r, given_data)
                    user.setScore(r)
                elif(Version == "WAF"):
                    given_data = { "token" : TOKEN,"version" : Version }
                    # check header
                    SCORE += check_header(2,Version,user_response,0)
                    # give scroe to user
                    user.score = SCORE
                    user.setScore(r)
                elif(Version == "Refund"):
                    SCORE = 0
                    user.score = SCORE
                    user.setScore(r)
                print(user.username)
                print(user.endpoint)
                # print(user_response)
                if(DEBUG_MODE == True):
                    print(user.error_ms)
                print("Give Score : "+str(user.score))
                print("------------------------------------------------")
            else:
                print("no user in Redis")
    
            # loop user
            index+=1
            if(index>=len(all_user)):
                index=0


def _threads_(): 
    threads = []
    for i in range(5):
        threads.append(threading.Thread(target=stresser,args=(str(i))))
        threads[i].start()

    for i in range(5):
        threads[i].join()
    # c= threading.Thread(target=stresser,args=("1")) 
    # d= threading.Thread(target=stresser,args=("2"))
    # a= threading.Thread(target=stresser,args=("3"))
    # e= threading.Thread(target=stresser,args=("4"))
    # z= threading.Thread(target=stresser,args=("5"))
    # c.start()
    # c.join()
    # d.start()
    # d.join()
    # a.start()
    # a.join()
    # e.start()
    # e.join()
    # z.start()
    # z.join()

def main():
	time.sleep(1)
	_threads_() 

main()
