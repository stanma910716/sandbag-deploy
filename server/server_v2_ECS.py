from flask import Flask,render_template
from flask import Response,request
import json
import requests
import hashlib
import time
import os
from datetime import datetime
from time import strftime

app = Flask(__name__)


name=""
count=0
@app.route('/', methods=['GET', 'POST'])
def home():
    global name
    global count
    try :
        count += 1
        name = request.headers["Token"]
        version = request.headers["Version"]
        file_location = "/var/log/sandbag.log"
        if(os.path.isfile(file_location)):
            file_exist = True
        else:
            open(file_location,"x")
        data = open(file_location,"r").read()
        with open(file_location,"w+") as log:
            nowtime = datetime.now().strftime('%m-%d-%Y %H:%M:%S')
            if(version == "Refund"):
                write_data = nowtime+" Warning: Unicorn Refund Request: "+name+"\n"
            if(version == "WAF"):
                write_data = nowtime+" Warning: Unicorn WAF Request: "+name+"\n"
            if(version == "v1" or version == "v2" or version == "v3"):
                write_data = nowtime+" Warning: Unicorn General Request: "+name+"\n"
            log.write(write_data+data)
        encrypt = hashlib.md5()
        encrypt.update(request.headers['Token'].encode("utf-8"))
        name = encrypt.hexdigest()
        time.sleep(3*count)
        response = Response()
        response.headers["Token"] = name
        response.headers["Version"] = "v2"
        response.headers["From"] = "ECS"
        response.headers["restime"] = str(datetime.now())
        count = count - 1
        return response
    except:
        failed = 1
    html = requests.get("https://stan-sandbag.s3.amazonaws.com/server_V2_ECS.html")
    return html.text

@app.route("/healthcheck", methods=["GET"])
def healthcheck():
    global count
    if(count >= 5):
        data = "Outstanding Request : "+str(count-5)
    else:
        data = "Outstanding Request : 0"
    return data

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)



