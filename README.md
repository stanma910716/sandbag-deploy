# Sandbag deploy template

### Code Review
- `/server` for **Test** to receive request 
(NoName)
- `/sandbag` for **Platform Console** to provide everyone login 
(xxx-Application)
- `/client` for **Request Server** to Send request to Tester 
( xxx-Server )

### Manual Setup ( Platform Console )
1. git clone
2. run `Platform Console` with command like this
``` ssh
python3 sandbag/manage.py runserver 0.0.0.0:80
```
3. Visit Server IP with **http protocol**
4. Use `admin / 123456789` to **Login** console

### Manual Setup ( Request Server )
1. git clone
2. run `Request Server` with command like this
``` ssh
python3 sandbag/client/client_multi.py
```
3. You can control **Request Speed** & **Request Power** in Platform Console anytime you want.

### Manual Setup ( Tester )
1. git clone `Tester File`
2. run `Tester Application` with command like this
``` ssh
sudo python sandbag/server/server_v2_ECS.py
```

### Update Version
- 2020/07/31 - `Add Challenge Page & merge new API`
- 2020/08/03 - `Add Version Change Controller`