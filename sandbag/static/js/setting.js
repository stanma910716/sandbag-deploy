var resp_data;
$(document).ready(function(){
  $.get(
    "/ajax_request_delay",
    function(data){
      resp_data = data;
      console.log(data);
      data = JSON.parse(data)["data"]["request_delay"];
      $("#request_speed").val(data);
    }
  )
  $.get(
    "/ajax_enable",
    function(data){
      data = JSON.parse(data);
      if(data.power == "on"){
        $("#power-switch").prop("checked",true);
      }else{
        $("#power-switch").prop("checked",false);
      }
    }
  )
  $.get(
    "/waf_status",
    function(data){
      data = JSON.parse(data);
      if(data.waf == "on"){
        $("#waf-switch").prop("checked",true);
      }else{
        $("#waf-switch").prop("checked",false);
      }
    }
  )
  $.get(
    "/ajax_version",
    function(data){
      data = JSON.parse(data);
      $("#request_version").val(data.version)
    }
  )
});
// Date time for type and version
// $('#datetimepicker1').datetimepicker({
//     defaultDate: new Date(),
//     format: 'DD/MM/YYYY H:mm',
//     sideBySide: true
// });
// $('#datetimepicker2').datetimepicker({
//     defaultDate: new Date(),
//     format: 'DD/MM/YYYY H:mm',
//     sideBySide: true
// });




var inputValue = 1
var inputStep = 1
// Token button 
$("#power-switch").change(function(){
  if($(this).prop("checked") == true){
    $.post(
      "/ajax_enable",
      "enable=on",
      function(data){
        data = JSON.parse(data);
        if(data.success == true){
          Swal.fire("Power On", "", "success");
        }else{
          Swal.fire("Turn On Power Failed", "", "error");
        }
      }
    )
  }else{
    $.post(
      "/ajax_enable",
      "enable=off",
      function(data){
        data = JSON.parse(data);
        if(data.success == true){
          Swal.fire("Power Off", "", "success");
        }else{
          Swal.fire("Turn Off Power Failed", "", "error");
        }
      }
    )
  }
});
$("#waf-switch").change(function(){
  if($(this).prop("checked") == true){
    $.post(
      "/waf_status",
      "enable=on",
      function(data){
        data = JSON.parse(data);
        if(data.success == true){
          Swal.fire("WAF Detect On", "", "success");
        }else{
          Swal.fire("Turn On WAF Detect Failed", "", "error");
        }
      }
    )
  }else{
    $.post(
      "/waf_status",
      "enable=off",
      function(data){
        data = JSON.parse(data);
        if(data.success == true){
          Swal.fire("WAF Detect Off", "", "success");
        }else{
          Swal.fire("Turn Off WAF Detect Failed", "", "error");
        }
      }
    )
  }
});
$("#submit-request-speed").click(function(){
  $.post(
    "/ajax_request_delay",
    "speed="+$("#request_speed").val(),
    function(data){
      data = JSON.parse(data);
      if(data.success == true){
        Swal.fire("Setting Success", "", "success");
      }else{
        Swal.fire("Setting Failed", "", "error");
      }
    }
  )
});
$("#submit-token").click(function(){
  Swal.fire({
    title: 'Create Token',
    html: `<input type="number" value="${inputValue}" step="${inputStep}" class="swal2-input" id="swal2-range-value">`,
    input: 'range',
    customClass: 'swal-height',
    inputValue,
    inputAttributes: {
      min: 0,
      max: 100,
      step: inputStep
    },
      onOpen: () => {
      const inputRange = Swal.getInput()
      const inputNumber = Swal.getContent().querySelector('#swal2-range-value')
      const button = Swal.getActions();
      inputRange.addEventListener ('input', () => {
        inputNumber.value=inputRange.value;
      });
      inputNumber.addEventListener ('change', () => {
        inputRange.value=inputNumber.value;
        inputRange.nextElementSibling.value=inputNumber.value;
      });
      button.addEventListener('click',()=>{
        console.log(inputNumber.value);
        $.ajax({
            url: "/ajax_token",
            type: 'POST',
            data: { "token":inputNumber.value,
            },
            dataType: 'json',
            success: function(data) {
                console.log("success")   
            }
            });
      });
    }
  })
});
$('#check-token').on('click',function(){
  //if() not disabled can do....
  var link = document.createElement("a");
  link.href = '/token';
  link.style = "visibility:hidden";
  link.target = "_blank";
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link); 
});
$("#submit-request-version").click(function(){
  $.post(
    "/ajax_version",
    "version="+$("#request_version").val(),
    function(data){
      data = JSON.parse(data);
      if(data.success == true){
        Swal.fire("Setting Success", "", "success");
      }else{
        Swal.fire("Setting Failed", "", "error");
      }
    }
  )
});