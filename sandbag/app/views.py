from django.shortcuts import render, redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import user_passes_test
from users.models import User,Token
import requests
import json




@user_passes_test(lambda u: u.is_staff,login_url='/')
@user_passes_test(lambda u: not u.is_superuser,login_url='/')
def index(request):
    context = {"index_page": "active"} 
    return render(request, 'index.html',context)

@user_passes_test(lambda u: u.is_staff,login_url='/')
def chart(request):
    context = {"chart_page": "active"}
    return render(request, 'chart.html',context)


@user_passes_test(lambda u: u.is_superuser,login_url='/')
def setting(request):
    context = {"setting_page": "active"}
    return render(request, 'setting.html',context)


@user_passes_test(lambda u: u.is_superuser,login_url='/')
def token(request):
    return render(request, 'setting-token.html')


@user_passes_test(lambda u: u.is_superuser,login_url='/')
def challenge_setting(request):
    context = {"challenge_setting_page": "active"}
    return render(request, 'challenge-setting.html',context)

@user_passes_test(lambda u: u.is_staff,login_url='/')
def challenge(request):
    all_quiz = json.loads(requests.get("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/get_quiz").text)
    context = {
        "challenge_page": "active",
        "all_quiz":all_quiz,
        "test":"test"
    } 
    return render(request, 'challenge.html',context)

@user_passes_test(lambda u: u.is_staff,login_url='/')
def challenge_blank(request,slug):
    sendData = {
        "slug" : str(slug)
    }
    data = (requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/get_quiz_by_slug",json=sendData))
    if(len(json.loads(data.text)) <= 0):
        return HttpResponseRedirect("/challenge/")
    context = {
        "quiz_slug":json.loads(data.text)[0]["quiz_slug"],
        "quiz_name":json.loads(data.text)[0]["quiz_name"],
        "fields":(json.loads(data.text)[0]["field"])
    }
    return render(request, 'challenge/blank.html',context)

