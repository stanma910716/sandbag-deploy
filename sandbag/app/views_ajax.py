from django.views.decorators.csrf import csrf_exempt
from django.db import models
from django.http import HttpResponse
import json
from users.models import Token
from uuid import uuid4
import requests
from django.conf import settings
import redis

#------function---------------

def set_endpoint(username,endpoint):
    settings.CACHE.lset(username+'_user', 0,endpoint) 
    print("save endpoint")

def set_translate_api(username,ans):
    settings.CACHE.lset(username+'_user', 2,ans) 
    print("save...")

#----------------------------

@csrf_exempt
def sandept(request):
    # Type : request -> Json 
    """
    
    """
    endpoint = request.POST.get('endpoint', None)
    context = {"result":endpoint}
    username = request.user.username
    set_endpoint(username,endpoint[1:-1])
    return HttpResponse(json.dumps(context))

@csrf_exempt
def power(request):
    # Type : request -> Json 
    """
    
    """
    username = request.user.username
    if( username == "admin"):
        if(request.method == "POST"):
            power = request.POST.get("enable","on")
            run = settings.CACHE.set("power",power)
            if(run):
                data = {"success":True}
            else:
                data = {"success":False}
            return HttpResponse(json.dumps(data))
        else:
            power = settings.CACHE.get("power")
            data = {"power":power}
            return HttpResponse(json.dumps(data))
    else:
        return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def waf_status(request):
    # Type : request -> Json 
    """
    
    """
    username = request.user.username
    if( username == "admin"):
        if(request.method == "POST"):
            waf = request.POST.get("waf","on")
            run = settings.CACHE.set("waf",waf)
            if(run):
                data = {"success":True}
            else:
                data = {"success":False}
            return HttpResponse(json.dumps(data))
        else:
            waf = settings.CACHE1.get("waf")
            if(waf == None):
                settings.CACHE.set("waf","on")
            waf = settings.CACHE1.get("waf")
            data = {"waf" : waf}
            # data = {"waf":waf}
            return HttpResponse(json.dumps(data))
    else:
        return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def version(request):
    # Type : request -> Json
    """

    """
    username = request.user.username
    if (username == "admin"):
        if(request.method == "GET"):
            if( settings.CACHE1.exists("request_version") ):
                request_version = settings.CACHE.get("request_version")
            else:
                settings.CACHE.set("request_version","v1")
                request_version = "v1"
            data = {
                "version":request_version
            }
            return HttpResponse(json.dumps(data))
        elif(request.method == "POST"):
            version = request.POST.get("version","v1")
            settings.CACHE.set("request_version",version)
            data = {
                "success" : True
            }
            return HttpResponse(json.dumps(data), status=200)
    else:
        return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def check(request):
    # Type : request -> Json 
    """
    
    """
    username = request.user.username
    user_token = request.user.token
    if(request.method == "POST"):
        if(request.POST.get("token") == None):
            token = request.POST.get("your_token")
            sendData = {
                "quiz_slug" : request.POST.get("quiz_slug"),
                "token" : request.POST.get("your_token"),
                "uuid" : user_token
            }
            r = requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/check_token",json=sendData)
            if(json.loads(r.text)["success"] == True):
                if("callback_url" in json.loads(r.text)):
                    sendData = json.loads(json.dumps(request.POST))
                    r1 = requests.post(json.loads(r.text)["callback_url"],json=sendData)
                    if(json.loads(r1.text)["success"] == True):
                        data = {
                            "success" : True
                        }
                    else:
                        data = {
                            "success" : False
                        }
            else:
                data = {
                    "success" : False
                }
        else:
            token = request.POST.get("token")
            sendData = {
                "quiz_slug" : request.POST.get("quiz_slug"),
                "token" : request.POST.get("token"),
                "uuid" : user_token
            }
            r = requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/check_token",json=sendData)
            if(json.loads(r.text)["success"] == True):
                if("callback_url" in json.loads(r.text)):
                    r1 = requests.get(json.loads(r.text)["callback_url"])
                    if(json.loads(r1.text)["success"] == True):
                        if( "check_url" in json.loads(r1.text)):
                            r2 = requests.get(json.loads(r1.text)["check_url"])
                            if(r2.status_code == 200):
                                data = {
                                    "success":True
                                }
                            else:
                                data = {
                                    "success":False,
                                    "error":1
                                }
                        else:
                            data = {
                                "success":True
                            }
                    else:
                        data = {
                            "success":False,
                            "error":2
                        }
                else:
                    data = {
                        "success":True
                    }
            else:
                data = {
                    "success" : False,
                    "error":3
                }
        if(data["success"] == True):
            sendData = {
                "quiz_slug" : request.POST.get("quiz_slug"),
                "token" : token,
                "uuid" : user_token
            }
            data = requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/success",json=sendData)
            return HttpResponse((data.text))
            if(json.loads(data.text)["success"]):
                data = {
                    "success":True
                }
            else:
                data = {
                    "success":False
                }
        return HttpResponse(json.dumps(data), status=200)
    else:
        return HttpResponse('Unauthorized', status=401)

def get_quiz(request):
    # Type : request -> Json 
    """
    
    """
    username = request.user.username
    user_token = request.user.token
    if(request.method == "GET"):
        r = requests.get("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/get_quiz")
        if(r.status_code == 200):
            return HttpResponse((r.text),status=200)
        else:
            return HttpResponse("test",status=200)
        # return HttpResponse(json.dumps(data), status=200)
    else:
        return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def auto_check(request):
    # Type : request -> Json 
    """
    
    """
    username = request.user.username
    user_token = request.user.token
    if(request.method == "GET"):
        sendData = {
            "uuid" : user_token
        }
        r = requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/check_user_quiz",json=sendData)
        return HttpResponse((r.text), status=200)
    else:
        return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def request_delay(request):
    # Type : request -> Json 
    """
    
    """
    with open('/home/ec2-user/sandbag-deploy/redis.endpoint', 'r') as files:
        redis_endpoint = str(files.read()).replace('[', '').replace(']', '').replace(' ', '').split(',')
    r = redis.Redis(host=redis_endpoint[0], port=6379, charset="utf-8",decode_responses=True)
    r1 = redis.Redis(host=redis_endpoint[2] , port=6379, charset="utf-8",decode_responses=True) 
    username = request.user.username
    if(username == "admin"):
        if(request.method == "GET"):
            if(r1.exists("request_delay") != True):
                r.set("request_delay",1.0)
            request_delay = r1.get("request_delay")
            data = {"data" : { "request_delay" : request_delay }}
            return HttpResponse(json.dumps(data))
        elif(request.method == "POST"):
            speed = request.POST.get("speed","0.0")
            run = r.set("request_delay",float(speed))
            if(run):
                data = {"success":True}
            else:
                data = {"success":False}
            return HttpResponse(json.dumps(data))
    else:
        return HttpResponse('Unauthorized', status=401)
    

@csrf_exempt
def crt_token(request):
    token = int(request.POST.get('token', None))
    context = {"result":token}
    for i in range(token):
        rand_token = uuid4()
        db = Token(token=rand_token, username='')
        db.save()
    return HttpResponse(json.dumps(context))

@csrf_exempt
def delete_token(request):
    token = (request.POST.get('token', None))
    if(token != None and token != "123456789"):
        unit = Token.objects.get(token=token)
        unit.delete()
        return HttpResponse(token)
    else:
        return HttpResponse("",status=401)

@csrf_exempt
def translept(request):
    endpoint = request.POST.get('endpoint', None)
    endpoint = endpoint[1:-1]
    username = request.user.username
    print(endpoint)
    # endpoint = "https://h1e54y0sel.execute-api.us-east-1.amazonaws.com/dev/translate-text"
    payload = {'API_Endpoint': endpoint}
    result="Your API is timeout. Pleas try again"
    testText = 'Now you can adjust the shade of the screen from white light to a warm amber with the ability to schedule when the light changes for a personalized reading experience. Kindle Oasis also has an adaptive front light that automatically adjusts the brightness of your screen based on lighting conditions.'
    try:
        req = requests.get('https://os99wyk3t1.execute-api.us-east-1.amazonaws.com/view/translate?sl={}&tl={}&text={}&url={}'.format('en', 'zh-TW', testText, endpoint))
        result = req.json()["Text"]
    except:
        pass
    if(result=="Your API is working perfectly."):
        set_translate_api(username,"true")
    else:
        set_translate_api(username,"false")
    context = {"result":result}
    return HttpResponse(json.dumps(context))
