from django.urls import path
from . import views, views_ajax

urlpatterns = [
    path('index/', views.index, name='index'),
    path('chart/', views.chart, name='chart'),
    path('setting/', views.setting, name='setting'),
    path('token/',views.token,name="token"),
    path('challenge_setting/',views.challenge_setting,name="challenge_setting"),
    path('challenge/',views.challenge,name="challenge"),
    # path('challenge/s3_version',views.challenge_s3_version,name="challenge_s3_version"),
    # path('challenge/ecs_practice',views.challenge_ecs,name="challenge_ecs"),
    path('ajax_enable',views_ajax.power,name='power'),
    path('waf_status',views_ajax.waf_status,name='waf_status'),
    path('ajax_version',views_ajax.version,name='version'),
    path('ajax_sandept',views_ajax.sandept,name='ajax_sandept'),
    path('ajax_request_delay',views_ajax.request_delay,name='request_delay'),
    path('delete_token',views_ajax.delete_token,name='delete_token'),
    path('ajax_token',views_ajax.crt_token,name='ajax_token'),
    path('ajax_translept',views_ajax.translept,name='ajax_translept'),
    path('get_quiz',views_ajax.get_quiz,name="ajax_get_quiz"),
    path('auto_check',views_ajax.auto_check,name="auto_check"),
    path('challenge/check',views_ajax.check,name='check'),
    path('challenge/<slug:slug>',views.challenge_blank,name="challenge_content"),
]
